from bs4 import BeautifulSoup
from sqlalchemy import create_engine
import psycopg2
from dotenv import dotenv_values


def dom_lineman(owner, period, data_html, chapter_name, recursive=False):
    bookmarks = []
    data_html = data_html.find_all('dt', recursive=recursive)
    for i in data_html:
        new_chapter = i.find('h3')
        if new_chapter:
            new_chapter_name = new_chapter.text if chapter_name == '' else f'{chapter_name}/{new_chapter.text}'
            bookmarks += dom_lineman(owner, period, i.find_next('dl'), new_chapter_name)
            continue
        bookmarks.append((period, owner, chapter_name, i.a.text, i.a.get('href')))
    return bookmarks


def load_records_to_db(records: list):
    conn_str = dotenv_values('.env').get('conn_str')
    engine = create_engine(conn_str)
    with engine.connect() as connection:
        for i in records:
            req = """
                insert into swamp.chrome_bookmarks (dt, user_name, chapter, bk_name, url)
                values
                (%s, %s, %s, %s, %s)
            """
            connection.execute(req, i)


filename = './bookmarks/bookmarks_01.01.2024.html'
with open(filename, 'r') as file:
    html = file.read()

soup = BeautifulSoup(html, 'html5lib').body.dl

chapter = ''
owner_name = 'lexx'
period_str = '2024-01-01'

result = dom_lineman(owner_name, period_str, soup, chapter)
load_records_to_db(result)

